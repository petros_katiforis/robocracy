# Robocracy

Robocracy is a top-down puzzle platformer in a post-apocalyptic world
in which robots have taken control. The actual goal is to figure out a
four digit code based on hints that you gather all around the map.

The game is built using C and SDL2. It's free software and distributed
under the GPLv3 license. You can reskin the game, design your own
levels or extend the engine's functionality. If you ever manage to
create something interesting with it, please let me know! I would love
to see what you've done.

## Technical details

The actual code is contained in a single ``main.c`` file. The game
utilizes Pioyi Engine which is a set of multi-purpose helpers that I
designed some months ago to help me create games faster. The game's
files contain a ``DEBUG_MODE`` flag which you can turn off and on to
get access to a barebones in-game level editor. Map, dialogue and some
key bindings data are stored in ``data.h``.

![Screenshot of robocracy gameplay](./screenshots/first.png)
![Screenshot of robocracy gameplay](./screenshots/second.png)
![Screenshot of robocracy gameplay](./screenshots/third.png)
