/* Robocracy
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Defining this will result in the game being compiled in debug mode
//#define DEBUG_MODE

#include <stdio.h>
#include <stdbool.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include "engine/game.h"
#include "engine/custom_math.h"
#include "engine/audio.h"
#include "engine/sprite.h"
#include "engine/interface.h"
#include "engine/tween.h"
#include "data.h"

#define FPS 60
#define DELTA_TIME (1.0 / FPS)

typedef enum
{
    DIRECTION_UP,
    DIRECTION_RIGHT,
    DIRECTION_DOWN,
    DIRECTION_LEFT
} direction_e;

typedef struct
{
    bool is_alive;
    vec2i_t tile_cords;
    direction_e direction;
} bullet_t;

/*
 * Some global variables that will be accessible throughout this file
 * All game logic will simply be implemented in a single file
 */
ng_game_t game;
TTF_Font *main_font;
bool has_game_finished = false;

// These will be toggled after each tick
// They will be used for simple animations
bool enemy_ticks_toggle, bullet_ticks_toggle;

unsigned int total_map_bolts = 0;
unsigned int total_bolts;
ng_label_t bolts_label;
room_t *cur_room;
bool is_paused = false;
// Auxiliary rectangles for rendering any tile
SDL_Rect temp_tile = {0, 0, 32, 32};
SDL_Rect temp_src = {0, 0, 32, 32};

SDL_Texture *player_texture, *tilemap_texture, *paper_texture, *closeups_texture, *interface_texture;
Mix_Chunk *door_sfx, *paper_sfx, *death_sfx, *bolt_sfx, *button_sfx, *error_sfx, *shotgun_sfx;

ng_label_t paper_label;
ng_sprite_t paper;
ng_tween_t *paper_tween;

vec2i_t spawn_position = {TILEMAP_WIDTH / 2, TILEMAP_HEIGHT / 2};
vec2i_t player_tile_cords = {TILEMAP_WIDTH / 2, TILEMAP_HEIGHT / 2};
direction_e player_fasing = DIRECTION_UP;
vec2i_t tilemap_offset;

// Fade in and fade out effects
float fade_opacity = 255;
SDL_Rect fade_rect = {0, 0};
ng_tween_t *fade_tween;

float dialogue_y;
SDL_Rect dialogue_background;
ng_tween_t *dialogue_tween;
ng_label_t dialogue_label;
ng_sprite_t dialogue_closeup;
bool is_dialogue_in_progress = false;
bool has_dialogue_frame_finished = false;
dialogue_line_t *dialogue_lines = NULL;
unsigned int dialogue_frame = 0;
unsigned int dialogue_chars_shown = 0;
unsigned int total_dialogue_lines = 0;
// Dynamically controls how long to wait before typing the next character
unsigned int dialogue_char_duration = 40;
char dialogue_content[DIALOGUE_MAX_LEN];
ng_timer_t dialogue_timer;

#define MAX_BULLETS 30
bullet_t bullets[MAX_BULLETS];

bool is_inserting_password = false;
ng_label_t password_label;
#define PASSWORD_PREFIX_LEN 12
char password_content[PASSWORD_PREFIX_LEN + 5] = "[Password]: ";
unsigned int password_length = PASSWORD_PREFIX_LEN;

bool are_credits_visible = false;
ng_label_t credits_label;

// Automatically positions the password label based on its length
void reposition_password_label(void)
{
    ng_transform_set_x(&password_label.sprite.transform,
            tilemap_offset.x + TILEMAP_WIDTH * 32 - password_label.sprite.transform.rect.w);
}

// Updates the room data and the texture
void room_set_tile(room_t * room, unsigned int x, unsigned int y, tile_e tile)
{
    room->data[y][x] = tile;
    SDL_SetRenderTarget(game.renderer, room->texture);

    temp_tile.x = x * 32;
    temp_tile.y = y * 32;

    if (tile == TILE_PAPER)
    {
        room->paper_position.x = x;
        room->paper_position.y = y;
    }
    
    // If it's void space, then just render a background rectangle on top of
    // the previous block Also if it's a tile of small size (e.g. floating
    // platforms) this procedure should also be executed
    if (tile == TILE_VOID || tile == TILE_FLOAT_ACTIVE || tile == TILE_FLOAT_INACTIVE ||
            tile == TILE_BRIDGE_VOID || tile == TILE_BRIDGE_SAFE)
    {
        SDL_SetRenderDrawColor(game.renderer, 10, 10, 10, 255);
        SDL_RenderFillRect(game.renderer, &temp_tile);
    }

    if (tile != TILE_VOID)
    {
        temp_src.x = (tile - 1) * 32;
        SDL_RenderCopy(game.renderer, tilemap_texture, &temp_src, &temp_tile);
    }
    
    SDL_SetRenderTarget(game.renderer, NULL);
}

void reset_map_state(void)
{
    for (int i = 0; i < TILEMAP_HEIGHT; i++)
    {
        for (int j = 0; j < TILEMAP_WIDTH; j++)
        {
            switch (cur_room->data[i][j])
            {
            case TILE_FLOAT_INACTIVE:
                room_set_tile(cur_room, j, i, TILE_FLOAT_ACTIVE);
                break;

            case TILE_BRIDGE_SAFE:
                room_set_tile(cur_room, j, i, TILE_BRIDGE_VOID);
                break;

            case TILE_BUTTON_PRESSED:
                room_set_tile(cur_room, j, i, TILE_BUTTON_UNPRESSED);
                break;
            }
        }
    }
}

void kill_player(void)
{
    is_paused = true;
    ng_tween_start(fade_tween, 0, 1000);
    ng_audio_play(death_sfx);
}

void dialogue_set_frame(unsigned int frame_index)
{
    dialogue_frame = frame_index;
    dialogue_chars_shown = 0;
    has_dialogue_frame_finished = false;
    memset(dialogue_content, 0, sizeof(char) * DIALOGUE_MAX_LEN);
    
    // Update the closeup based on the character
    ng_sprite_set_frame(&dialogue_closeup, dialogue_lines[dialogue_frame].character);
    ng_timer_restart(&dialogue_timer);
}

void dialogue_update_positions(void)
{
    dialogue_background.y = (int) dialogue_y;
    ng_transform_set_y(&dialogue_closeup.transform, dialogue_y + 20);
    ng_transform_set_y(&dialogue_label.sprite.transform, dialogue_y + 20);
    ng_timer_start(&dialogue_timer);
}

#define PROMPT_DIALOGUE(array) prompt_dialogue(array, sizeof(array) / sizeof(dialogue_line_t))
void prompt_dialogue(dialogue_line_t *lines, unsigned int total_lines)
{
    total_dialogue_lines = total_lines;
    dialogue_lines = lines;
    ng_tween_start(dialogue_tween, game.height - dialogue_background.h, 600);
    is_paused = true;
    is_dialogue_in_progress = true;

    dialogue_set_frame(0);
}

// Checks if the tile cord is inside the map range
bool are_tile_cords_valid(int tile_x, int tile_y)
{
    return tile_x >= 0 && tile_x < TILEMAP_WIDTH && tile_y >= 0 && tile_y < TILEMAP_HEIGHT;
}

bool bullet_has_hit_obstacle(bullet_t *bullet)
{
    // Check if the bullet position is actually valid
    if (!are_tile_cords_valid(bullet->tile_cords.x, bullet->tile_cords.y)) return true;
    
    switch (cur_room->data[bullet->tile_cords.y][bullet->tile_cords.x])
    {
    case TILE_WALL_TOP:
        return true;
    }

    return false;
}

// Returns true if the tile is walkable by the player
bool tile_is_walkable(tile_e tile)
{
    return !(tile == TILE_BRIDGE_VOID || tile == TILE_VOID || (tile >= TILE_GUN_UP && tile <= TILE_GUN_LEFT)
             || tile == TILE_WALL_TOP || tile == TILE_FLOAT_INACTIVE || tile == TILE_PASSWORD_DOOR);
}

void room_texture_draw_spikes(room_t *room)
{
    SDL_SetRenderTarget(game.renderer, room->texture);
    temp_src.x = (TILE_SPIKES - 1 + enemy_ticks_toggle) * 32;
    
    for (int y = 0; y < TILEMAP_HEIGHT; y++)
    {
        for (int x = 0; x < TILEMAP_WIDTH; x++)
        {
            if (room->data[y][x] == TILE_SPIKES)
            {
                temp_tile.x = x * 32;
                temp_tile.y = y * 32;

                SDL_RenderCopy(game.renderer, tilemap_texture, &temp_src, &temp_tile);
            }
        }
    }
    
    SDL_SetRenderTarget(game.renderer, NULL);
}

// generate_room_texture helper function
void draw_arrow_if_needed(room_t *current, room_e other_id, int x, int y, int rotation)
{
    if (other_id == ROOM_INVALID) return;
    
    current->data[y][x] = TILE_ARROW;
    temp_tile.x = x * 32;
    temp_tile.y = y * 32;

    SDL_RenderCopyEx(game.renderer, tilemap_texture, &temp_src, &temp_tile, rotation, NULL, SDL_FLIP_NONE);
}

// Generates the texture from the room data and stores it inside the struct
void generate_room_texture(room_t *room)
{
    room->texture = SDL_CreateTexture(game.renderer, SDL_PIXELFORMAT_RGBA8888,
                                      SDL_TEXTUREACCESS_TARGET, TILEMAP_WIDTH * 32, TILEMAP_HEIGHT * 32);
        
    // Rendering on the texture instead on the actual window
    SDL_SetRenderTarget(game.renderer, room->texture);
    SDL_SetRenderDrawColor(game.renderer, 10, 10, 10, 255);
    SDL_RenderClear(game.renderer);

    room->paper_position.x = -1;
    
    for (int y = 0; y < TILEMAP_HEIGHT; y++)
    {
        for (int x = 0; x < TILEMAP_WIDTH; x++)
        {
            // Spikes will be rendered during every tick because they change
            if (room->data[y][x] == TILE_VOID || room->data[y][x] == TILE_SPIKES) continue;

            if (room->data[y][x] == TILE_PAPER)
            {
                room->paper_position.x = x;
                room->paper_position.y = y;
            }
            else if (room->data[y][x] == TILE_PLATFORM_BOLT)
            {
                total_map_bolts++;
            }
            
            temp_tile.x = x * 32;
            temp_tile.y = y * 32;

            // Selecting the appropriate part of the tilemap texture based on the tile type
            temp_src.x = (room->data[y][x] - 1) * 32;

            SDL_RenderCopy(game.renderer, tilemap_texture, &temp_src, &temp_tile);
        }
    }

    // Creating the room navigation arrows
    temp_src.x = (TILE_ARROW - 1) * 32;

    draw_arrow_if_needed(room, room->up, TILEMAP_WIDTH / 2, 0, -90);
    draw_arrow_if_needed(room, room->down, TILEMAP_WIDTH / 2, TILEMAP_HEIGHT - 1, 90);
    draw_arrow_if_needed(room, room->right, TILEMAP_WIDTH - 1, TILEMAP_HEIGHT / 2, 0);
    draw_arrow_if_needed(room, room->left, 0, TILEMAP_HEIGHT / 2, 180);

    room_texture_draw_spikes(room);
    SDL_SetRenderTarget(game.renderer, NULL);
}

void set_room(room_e room_id)
{
    // Do something if it's the first time we're visiting the room
    cur_room = &rooms[room_id];

    if (!cur_room->has_been_visited)
    {
        switch (room_id)
        {
        case ROOM_START:
            PROMPT_DIALOGUE(intro_dialogue);
            break;

        case ROOM_MAZE:
            PROMPT_DIALOGUE(escape_dialogue);
            break;
            
        // This lets the player know that they can hit 'r' to respawn
        case ROOM_FLOATING_PLATFORMS:
            PROMPT_DIALOGUE(respawn_dialogue);
            break;

        case ROOM_DEADEND:
            PROMPT_DIALOGUE(gut_feeling_dialogue);
            room_set_tile(&rooms[ROOM_SPIKES], 2, 1, TILE_PAPER);
            
            break;
        }
    }

    cur_room->has_been_visited = true;
    
    if (cur_room->paper_content[0] != 0)
        ng_label_set_content(&paper_label, game.renderer, cur_room->paper_content);

    memcpy(&spawn_position, &player_tile_cords, sizeof(vec2i_t));

    // Clear out all bullets
    memset(&bullets, 0, sizeof(bullet_t) * MAX_BULLETS);

    reset_map_state();
}

void spawn_bullet(unsigned int tile_x, unsigned int tile_y, direction_e direction)
{
    // Find an empty spot inside the bullet pool
    for (int i = 0; i < MAX_BULLETS; i++)
    {
        if (bullets[i].is_alive) continue;
        
        bullets[i].is_alive = true;
        bullets[i].tile_cords.x = tile_x;
        bullets[i].tile_cords.y = tile_y;
        bullets[i].direction = direction;

        return;
    }
}

void set_bolts(unsigned int new_value)
{
    total_bolts = new_value;

    char label_content[20];
    sprintf(label_content, "Bolts: %d", new_value);
    ng_label_set_content(&bolts_label, game.renderer, label_content);
}

void initialize_globals(void)
{
    ng_game_create(&game, "Robocracy", (TILEMAP_WIDTH + 2) * 32, 640);
    
    // Loading up static assets, textures and audio
    player_texture = IMG_LoadTexture(game.renderer, "res/player.png");
    tilemap_texture = IMG_LoadTexture(game.renderer, "res/tilemap.png");
    paper_texture = IMG_LoadTexture(game.renderer, "res/paper.png");
    closeups_texture = IMG_LoadTexture(game.renderer, "res/closeups.png");
    interface_texture = IMG_LoadTexture(game.renderer, "res/interface.png");
    
    main_font = TTF_OpenFont("res/free_mono.ttf", 16);
    door_sfx = ng_audio_load("res/door.wav");
    paper_sfx = ng_audio_load("res/paper.wav");
    death_sfx = ng_audio_load("res/death.wav");
    bolt_sfx = ng_audio_load("res/bolt.wav");
    button_sfx = ng_audio_load("res/button.wav");
    error_sfx = ng_audio_load("res/error.wav");
    shotgun_sfx = ng_audio_load("res/shotgun.wav");
    
    ng_sprite_create(&paper, paper_texture);
    ng_transform_set_position(&paper.transform, (game.width - paper.src.w) / 2, -paper.transform.rect.h);
    ng_label_create(&paper_label, main_font, paper.transform.rect.w - 50);
    ng_transform_set_position(&paper_label.sprite.transform, paper.transform.rect.x + 25, paper.transform.rect.y + 25);
    
    paper_tween = ng_tween_create(&paper.transform.position.y);
    fade_tween = ng_tween_create(&fade_opacity);
    dialogue_tween = ng_tween_create(&dialogue_y);

    // Centering the tilemap relative to the screen
    tilemap_offset.x = (game.width - TILEMAP_WIDTH * 32) / 2;
    tilemap_offset.y = (game.height - TILEMAP_HEIGHT * 32) / 2;

    dialogue_background.w = game.width;
    dialogue_background.x = 0;
    dialogue_background.y = tilemap_offset.y + TILEMAP_HEIGHT * 32;
    dialogue_background.h = 150;
    
    ng_sprite_create_with_frames(&dialogue_closeup, closeups_texture, 2);
    ng_transform_set_x(&dialogue_closeup.transform, tilemap_offset.x);
    ng_label_create(&dialogue_label, main_font, dialogue_background.w - 2 * tilemap_offset.x - dialogue_closeup.transform.rect.w - 10);
    ng_transform_set_x(&dialogue_label.sprite.transform, dialogue_closeup.transform.rect.x + dialogue_closeup.transform.rect.w + 10);

    ng_label_create(&password_label, main_font, 0);
    ng_transform_set_y(&password_label.sprite.transform, tilemap_offset.y - 35);
    
    dialogue_y = game.height;
    dialogue_update_positions();

    fade_rect.w = game.width;
    fade_rect.h = game.height;

    for (int i = ROOM_START; i < TOTAL_ROOMS; i++)
        generate_room_texture(&rooms[i]);

    char bolts_dialogue_content[DIALOGUE_MAX_LEN];
    sprintf(bolts_dialogue_content, "Hmm, the motherboard of the password validator is disconnected. "
            "I need %d bolts before I can use this machine to escape.", total_map_bolts);
    
    strcpy(not_enough_bolts_dialogue[0].content, bolts_dialogue_content);

    ng_label_create(&credits_label, main_font, game.width - 50);
    ng_label_set_content(&credits_label, game.renderer,
                         "Robocracy\n\n"
                         "Credits:\n"
                         " * Programming and pixel art by Petros Katiforis\n"
                         " * Sound effects from freesound.org\n\n"
                         "This game is free (libre) software under the GPLv3 license.\n"
                         "The source code is available here:\n"
                         "https://codeberg.org/petros_katiforis/robocracy");
    
    ng_transform_set_position(&credits_label.sprite.transform, (game.width - credits_label.sprite.transform.rect.w) / 2, 25);
    
    ng_label_create(&bolts_label, main_font, 0);
    ng_transform_set_position(&bolts_label.sprite.transform, tilemap_offset.x + 45, tilemap_offset.y - 35);
    set_bolts(0);
    set_room(ROOM_START);
}

void handle_key_down(SDL_Event *event)
{
    /*
     * A barebones map designer running exclusively in debug mode
     */
#ifdef DEBUG_MODE
    // If the user is inserting a password, just ignore the editing commands
    // because there are going to be some clashes
    if (is_inserting_password) return;
    
    int mouse_x, mouse_y;
    SDL_GetMouseState(&mouse_x, &mouse_y);
    
    // When in debug mode, check if the user is trying to edit tiles
    if (mouse_x > tilemap_offset.x && mouse_x <= tilemap_offset.x + TILEMAP_WIDTH * 32
        && mouse_y > tilemap_offset.y && mouse_y <= tilemap_offset.y + TILEMAP_HEIGHT * 32)
    {
        for (int i = 0; i < sizeof(editor_key_bindings) / sizeof(editor_key_binding); i++)
        {
            if (event->key.keysym.sym == editor_key_bindings[i].key)
            {
                room_set_tile(cur_room, (mouse_x - tilemap_offset.x) / 32, (mouse_y - tilemap_offset.y) / 32,
                        editor_key_bindings[i].tile);

                return;
            }
        }
    }
    
    // Just printing out the map data using a nice little format
    // This can be then verbatim copied and pasted to the data.h file
    if (event->key.keysym.sym == SDLK_SPACE)
    {
        for (int i = 0; i < TILEMAP_HEIGHT; i++)
        {
            printf("{");
            
            for (int j = 0; j < TILEMAP_WIDTH; j++)
            {
                printf("%d", cur_room->data[i][j]);

                if (j != TILEMAP_WIDTH - 1)
                    printf(", ");
            }
            
            printf("},\n");
        }

        return;
    }
#endif
    
    // Check if the player is trying to skip to the next dialogue frame
    if (is_dialogue_in_progress && event->key.keysym.sym == SDLK_d)
    {
        if (dialogue_frame != total_dialogue_lines - 1)
        {
            dialogue_set_frame(dialogue_frame + 1);
        }
        else
        {
            ng_tween_start(dialogue_tween, game.height, 300);

            // If the dialogue that just finished was the ending dialogue, start the fade effect
            if (has_game_finished)
            {
                ng_tween_start(fade_tween, 0, 2000);
            }
            else
            {
                is_paused = false;
            }

            is_dialogue_in_progress = false;
        }

        return;
    }

    // Respawning when the player pressed R
    tile_e cur_tile = cur_room->data[player_tile_cords.y][player_tile_cords.x];
    if ((!is_paused || cur_tile == TILE_PAPER) && event->key.keysym.sym == SDLK_r)
    {
        if (cur_tile == TILE_PAPER)
        {
            ng_tween_start(paper_tween, -paper.transform.rect.h, 300);
        }
        
        kill_player();
        return;
    }
    
    // If the game is paused, the player can only move only if he's reading a note
    // Otherwise there wouldn't be any way to exit reading
    if (is_paused && cur_tile != TILE_PAPER) return;
    
    // If it's none of the above, it might be movement
    SDL_Keycode key = event->key.keysym.sym;
    int x_direction = (key == SDLK_d) - (key == SDLK_a);
    int y_direction = (key == SDLK_s) - (key == SDLK_w);

    if (x_direction || y_direction)
    {
        // Check if we're inside the boundaries and if the tile is valid
        int new_x = player_tile_cords.x + x_direction;
        int new_y = player_tile_cords.y + y_direction;

        if (x_direction)
            player_fasing = (x_direction > 0) ? DIRECTION_RIGHT : DIRECTION_LEFT;

        if (y_direction)
            player_fasing = (y_direction > 0) ? DIRECTION_DOWN : DIRECTION_UP;

        if (!are_tile_cords_valid(new_x, new_y)) return;
        tile_e new_tile = cur_room->data[new_y][new_x];
        
        if (!tile_is_walkable(new_tile))
        {
            // Check if the player is trying to escape
            if (new_tile == TILE_PASSWORD_DOOR)
            {                           
                // The player needs to collect all map bolts before entering a password
                if (total_bolts != total_map_bolts)
                {
                    ng_audio_play(error_sfx);
                    PROMPT_DIALOGUE(not_enough_bolts_dialogue);
                }
                else
                {
                    password_length = 12;
                    password_content[password_length] = 0;
                    ng_label_set_content(&password_label, game.renderer, password_content);
                    reposition_password_label();                            
                    is_paused = true;
                    is_inserting_password = true;
                }
            }

            return;
        }
        
        // Before actually moving the player, we need to execute
        // some code based on the previous tile that we're now
        // leaving behind
        switch (cur_tile)
        {
        case TILE_PAPER:
            // If the paper was previously tweened and now the player
            // has left the block, get it off the screen
            is_paused = false;
            ng_tween_start(paper_tween, -paper.transform.rect.h, 300);

            break;

        case TILE_FLOAT_ACTIVE:
            // If the previous tile was floating, make it inactive.
            // It's a quite popular mechanic
            room_set_tile(cur_room, player_tile_cords.x, player_tile_cords.y, TILE_FLOAT_INACTIVE);
            break;
        }

        player_tile_cords.x = new_x;
        player_tile_cords.y = new_y;

        // Check if the player has been hit by a bullet
        for (int i = 0; i < MAX_BULLETS; i++)
        {
            if (bullets[i].is_alive && bullets[i].tile_cords.x == new_x && bullets[i].tile_cords.y == new_y)
            {
                kill_player();
                return;
            }
        }
        
        switch (new_tile)
        {
        case TILE_ARROW:
            // If we're on an arrow tile, go to the next room
            player_tile_cords.x = (TILEMAP_WIDTH - 1) - new_x;
            player_tile_cords.y = (TILEMAP_HEIGHT - 1) - new_y;

            // Figure out where the arrow is pointing at
            if (new_y == 0)                       set_room(cur_room->up);
            else if (new_y == TILEMAP_HEIGHT - 1) set_room(cur_room->down);
            else if (new_x == 0)                  set_room(cur_room->left);
            else if (new_x == TILEMAP_WIDTH - 1)  set_room(cur_room->right);

            ng_audio_play(door_sfx);
            
            break;

        // If the player reached a button, active the map's bridge
        case TILE_BUTTON_UNPRESSED:
            for (int i = 0; i < TILEMAP_WIDTH; i++)
            {
                for (int j = 0; j < TILEMAP_HEIGHT; j++)
                {
                    if (cur_room->data[i][j] == TILE_BRIDGE_VOID)
                        room_set_tile(cur_room, j, i, TILE_BRIDGE_SAFE);
                }
            }
            
            room_set_tile(cur_room, new_x, new_y, TILE_BUTTON_PRESSED);
            ng_audio_play(button_sfx);

            break;
            
        case TILE_PLATFORM_BOLT:
            set_bolts(total_bolts + 1);
            room_set_tile(cur_room, new_x, new_y, TILE_PLATFORM);
            ng_audio_play(bolt_sfx);
            
            break;

        // If the player reached a note, display it
        case TILE_PAPER:
            cur_room->has_read_paper = true;
            ng_label_set_content(&paper_label, game.renderer, cur_room->paper_content);
            ng_tween_start(paper_tween, 100, 600);
            ng_audio_play(paper_sfx);
            is_paused = true;
            
            break;
            
        case TILE_SPIKES:
            if (enemy_ticks_toggle)
                kill_player();

            break;
        }
    }
}

int main()
{
    initialize_globals();
    SDL_Event event;

    ng_interval_t bullet_ticks, enemy_ticks;
    ng_interval_create(&bullet_ticks, 125);
    ng_interval_create(&enemy_ticks, 700);
    
    SDL_Rect tilemap_rect = {tilemap_offset.x, tilemap_offset.y, TILEMAP_WIDTH * 32, TILEMAP_HEIGHT * 32};

    // This is where the game loop is defined
    for (;;)
    {
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
                goto cleanup;

            switch (event.type)
            {
            case SDL_TEXTINPUT:
                // Password inserting functionality
                if (!is_inserting_password) break;
                if (event.text.text[0] < '0' || event.text.text[0] > '9') break;

                password_content[password_length++] = event.text.text[0];
                password_content[password_length] = 0;
                ng_label_set_content(&password_label, game.renderer, password_content);
                reposition_password_label();

                // Check if the password is completed
                if (password_length == PASSWORD_PREFIX_LEN + 4)
                {
                    is_inserting_password = false;
                    is_paused = false;

                    char *password = password_content + PASSWORD_PREFIX_LEN;
                    // If it's wrong just play an error sound effect
                    if (strcmp(password, CORRECT_PASSWORD) != 0)
                    {
                        ng_audio_play(error_sfx);
                    }
                    else
                    {
                        has_game_finished = true;
                        PROMPT_DIALOGUE(ending_dialogue);
                    }

                    goto update_and_render;
                }
                
                break;
                
            case SDL_KEYDOWN:
                handle_key_down(&event);
                break;
            }
        }

    update_and_render:
        /*
         * Updating and rendering the game's entities
         */
        // Process game ticks
        if (!is_paused && ng_interval_is_ready(&enemy_ticks))
        {
            enemy_ticks_toggle = !enemy_ticks_toggle;
            
            room_texture_draw_spikes(cur_room);
            // Check if there's any spike that just killed the player
            if (cur_room->data[player_tile_cords.y][player_tile_cords.x] == TILE_SPIKES && enemy_ticks_toggle)
                kill_player();

            // Spawn bullets from all guns if the toggle is on
            if (enemy_ticks_toggle)
            {
                for (int i = 0; i < TILEMAP_HEIGHT - 1; i++)
                {
                    for (int j = 0; j < TILEMAP_WIDTH; j++)
                    {
                        if (cur_room->data[i][j] >= TILE_GUN_UP && cur_room->data[i][j] <= TILE_GUN_LEFT)
                            spawn_bullet(j, i, cur_room->data[i][j] - TILE_GUN_UP);
                    }
                }
            }
        }
        
        if (!is_paused && ng_interval_is_ready(&bullet_ticks))
        {
            bullet_ticks_toggle = !bullet_ticks_toggle;
            
            // Moving the bullets
            for (int i = 0; i < MAX_BULLETS; i++)
            {
                if (!bullets[i].is_alive) continue;
                bullet_t *bullet = &bullets[i];
                
                // Destroy all bullets that have collided with an obstacle
                if (bullet->direction == DIRECTION_UP) bullet->tile_cords.y--;
                else if (bullet->direction == DIRECTION_DOWN) bullet->tile_cords.y++;
                else if (bullet->direction == DIRECTION_RIGHT) bullet->tile_cords.x++;
                else if (bullet->direction == DIRECTION_LEFT) bullet->tile_cords.x--;

                // Check if the player was hit
                if (bullet->tile_cords.x == player_tile_cords.x && bullet->tile_cords.y == player_tile_cords.y)
                {
                    kill_player();
                }
                
                if (bullet_has_hit_obstacle(bullet))
                {
                    bullet->is_alive = false;
                }
            }
        }

        // While the tween is active, call the update position function
        if (paper_tween->is_active)
        {
            // AFter the tween has finished, unpause the game
            ng_tween_update(paper_tween, DELTA_TIME * 1000);
            
            ng_transform_set_y(&paper.transform, *paper_tween->value);
            ng_transform_set_y(&paper_label.sprite.transform, *paper_tween->value + 25);
        }

        // Updating the dialogue text for a typewritter sort of look
        if (is_dialogue_in_progress && !has_dialogue_frame_finished && \
            (ng_timer_get_elapsed(&dialogue_timer) > dialogue_char_duration || dialogue_chars_shown == 0))
        {
            char character = dialogue_lines[dialogue_frame].content[dialogue_chars_shown];
            
            // Increment the text
            dialogue_content[dialogue_chars_shown] = character;
            dialogue_chars_shown++;

            // If the current character was punctuation make the delay a bit longer!
            if (character == '!' || character == '.' || character == '?' || character == ',')
                dialogue_char_duration = 300;
            else
                dialogue_char_duration = 10;
            
            ng_timer_restart(&dialogue_timer);
            
            // Check if we've reached the end of the current frame
            if (dialogue_lines[dialogue_frame].content[dialogue_chars_shown] == 0)
            {
                has_dialogue_frame_finished = true;
            }

            ng_label_set_content(&dialogue_label, game.renderer, dialogue_content);
        }
        
        /*
         * Rendering
         */
        SDL_SetRenderDrawColor(game.renderer, 10, 10, 10, 255);
        SDL_RenderClear(game.renderer);

        SDL_RenderCopy(game.renderer, cur_room->texture, NULL, &tilemap_rect);
            
        temp_tile.x = tilemap_offset.x + player_tile_cords.x * 32;
        temp_tile.y = tilemap_offset.y + player_tile_cords.y * 32;
        SDL_RenderCopy(game.renderer, player_texture, NULL, &temp_tile);

        // Rendering bolts
        temp_tile.x = tilemap_offset.x;
        temp_tile.y = tilemap_offset.y - 40;
        temp_src.x = (TILE_UI_BOLT - 1) * 32;
        SDL_RenderCopy(game.renderer, tilemap_texture, &temp_src, &temp_tile);
        ng_sprite_render(&bolts_label.sprite, game.renderer); 

        // If this map's note hasn't been read yet, show an exclamation mark
        // This is especially useful whenever notes are unlocked only after the
        // player has left their room of origin
        if (!cur_room->has_read_paper && cur_room->paper_position.x != -1)
        {
            temp_src.x = 0;
            temp_tile.x = tilemap_offset.x + cur_room->paper_position.x * 32 + 16;
            temp_tile.y = tilemap_offset.y + (cur_room->paper_position.y - 1) * 32 + 16;
            SDL_RenderCopy(game.renderer, interface_texture, &temp_src, &temp_tile);
        }
        
        // Rendering the bullets
        for (int i = 0; i < MAX_BULLETS; i++)
        {
            if (!bullets[i].is_alive) continue;
            bullet_t *bullet = &bullets[i];
            
            temp_tile.x = tilemap_offset.x + bullet->tile_cords.x * 32;
            temp_tile.y = tilemap_offset.y + bullet->tile_cords.y * 32;
            temp_src.x = (4 + bullet_ticks_toggle) * 32;
            SDL_RenderCopy(game.renderer, tilemap_texture, &temp_src, &temp_tile);
        }

        if (paper.transform.rect.y > -paper.transform.rect.h)
        {
            ng_sprite_render(&paper, game.renderer);
            ng_sprite_render(&paper_label.sprite, game.renderer);
        }

        if (dialogue_tween->is_active)
        {
            ng_tween_update(dialogue_tween, DELTA_TIME * 1000);
            dialogue_update_positions();
        }

        // Rendering the dialogue
        if (is_dialogue_in_progress || dialogue_tween->is_active)
        {
            SDL_SetRenderDrawBlendMode(game.renderer, SDL_BLENDMODE_BLEND);
            SDL_SetRenderDrawColor(game.renderer, 10, 10, 10, 220);
            SDL_RenderFillRect(game.renderer, &dialogue_background);
            
            ng_sprite_render(&dialogue_closeup, game.renderer);
            ng_sprite_render(&dialogue_label.sprite, game.renderer);
        }

        if (is_inserting_password)
        {
            ng_sprite_render(&password_label.sprite, game.renderer);
        }
        
        // Fade effect rectangle
        if (fade_tween->is_active)
        {
            if (ng_tween_update(fade_tween, DELTA_TIME * 1000))
            {
                // If the tween is over and the screen is black, start a new
                // tween to make it transparent again
                if (fade_opacity == 0)
                {
                    // When the fade is used after the game has finished,
                    // it should start a timer which will in its turn show the credits
                    if (has_game_finished)
                    {
                        ng_audio_play(shotgun_sfx);
                        are_credits_visible = true;
                    }
                    // This is for when the fade effect is used for player respawning
                    else
                    {
                        ng_tween_start(fade_tween, 255, 600);
                        reset_map_state();
                        memset(&bullets, 0, sizeof(bullet_t) * MAX_BULLETS);
                        memcpy(&player_tile_cords, &spawn_position, sizeof(vec2i_t));
                    }
                }
                // Otherwise, it means that the effect is done
                else
                {
                    is_paused = false;
                }
            }
        }
        
        if (fade_tween->is_active || has_game_finished)
        {
            SDL_SetRenderDrawBlendMode(game.renderer, SDL_BLENDMODE_BLEND);
            SDL_SetRenderDrawColor(game.renderer, 0, 0, 0, 255 - (int) fade_opacity);
            SDL_RenderFillRect(game.renderer, &fade_rect);
        }

        if (are_credits_visible)
        {
            ng_sprite_render(&credits_label.sprite, game.renderer);
        }
        
        SDL_RenderPresent(game.renderer);
        SDL_Delay(1000 / FPS);
    }

cleanup:
    ng_game_destroy(&game);
}
