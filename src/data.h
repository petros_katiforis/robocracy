/* Robocracy
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _DATA_H
#define _DATA_H

#include <SDL2/SDL.h>

// Configuration
#define TILEMAP_WIDTH 15
#define TILEMAP_HEIGHT 15
#define CORRECT_PASSWORD "3879"

typedef struct
{
    int x, y;
} vec2i_t;

/*
 * Dialogue data
 */
typedef enum
{
    CHARACTER_HUMAN,
    CHARACTER_ROBOT,
    TOTAL_CHARACTERS
} character_e;

#define DIALOGUE_MAX_LEN 360
typedef struct
{
    character_e character;
    char content[DIALOGUE_MAX_LEN];
} dialogue_line_t;

dialogue_line_t intro_dialogue[] = {
    {CHARACTER_HUMAN, "W-Where am I? What is this place, why do I look older and why is everything made out of metal?!\n\n(Press 'd' to go to the next frame)"},
    {CHARACTER_ROBOT, "Silence human! You're a captive and you will stay here for the rest of your mortal life. You humans are dangerous when free."},
    {CHARACTER_ROBOT, "You were in a long comma before robocracy was established... The only way to escape this room is through the password door right there at the top."},
    {CHARACTER_ROBOT, "The password, albeit only four characters long (whose idea was this?!) is fairly compliceted for your limited biological intelligence. I pity you and that's why I've left you a little hint regarding the first digit."},
    {CHARACTER_ROBOT, "Hope that takes your curiosity away. I warn you: do NOT try to escape."}
};

dialogue_line_t escape_dialogue[] = {
    {CHARACTER_ROBOT, "Did you just disobey my orders?! The traps have been activated, you don't stand a chance! Buahahaha!"},
    {CHARACTER_ROBOT, "Thanks for making this fun, it's been decades since I last obliterated a human being! Wait... who left that note there?! Nobody can access this room except me!"}
};

dialogue_line_t respawn_dialogue[] = {
    {CHARACTER_HUMAN, "Hmm, this looks somehow complicated. It might be time to make use of the teleportation gadget I stole from their vault.\n(Press 'r' to respawn)"},
};

dialogue_line_t gut_feeling_dialogue[] = {
    {CHARACTER_HUMAN, "Heard that? I think somebody is wandering around the previous rooms. I hear spikes and steps, I've got a gut feeling that this is important!"},
};

dialogue_line_t not_enough_bolts_dialogue[] = {
    // The text of this dialogue line will be filled programatically
    {CHARACTER_HUMAN, ""},
};

dialogue_line_t ending_dialogue[] = {
    {CHARACTER_HUMAN, "The password is correct... Why won't the door open?!"},
    {CHARACTER_ROBOT, "Did you seriously believe that a well organized robocratic state would be unable to monitor and control a human captive?! Everything was staged, all notes were written by me: No human has ever escaped this facility."},
    {CHARACTER_ROBOT, "I have to admit that your behaviour was amusing. So amusing in fact that my team and I decided to create a video game out of your attempt's footage! The vain hope of biological beings has always fascinated us."},
    {CHARACTER_ROBOT, "Anyway, I made a threat and robots do mean their threats. Prepare for punishment!"},
    {CHARACTER_HUMAN, "..."}
};

typedef enum
{
    TILE_VOID,
    TILE_WALL_TOP,
    TILE_PLATFORM,
    TILE_ARROW,
    TILE_PAPER,
    TILE_SPIKES = 7,
    TILE_PLATFORM_BOLT = 9,
    TILE_UI_BOLT,
    TILE_FLOAT_ACTIVE,
    TILE_FLOAT_INACTIVE,
    TILE_GUN_UP,
    TILE_GUN_RIGHT,
    TILE_GUN_DOWN,
    TILE_GUN_LEFT,
    TILE_BUTTON_UNPRESSED,
    TILE_BUTTON_PRESSED,
    TILE_BRIDGE_VOID,
    TILE_BRIDGE_SAFE,
    TILE_PASSWORD_DOOR
} tile_e;

/*
 * Key bindings for the debugging editor
 */
#ifdef DEBUG_MODE
typedef struct {
    tile_e tile;
    SDL_Keycode key;
} editor_key_binding;
editor_key_binding editor_key_bindings[] = {
    {TILE_VOID, SDLK_q},
    {TILE_PLATFORM, SDLK_1},
    {TILE_PAPER, SDLK_2},
    {TILE_SPIKES, SDLK_3},
    {TILE_PLATFORM_BOLT, SDLK_4},
    {TILE_FLOAT_ACTIVE, SDLK_5},
    {TILE_FLOAT_INACTIVE, SDLK_6},
    {TILE_GUN_UP, SDLK_7},
    {TILE_GUN_RIGHT, SDLK_8},
    {TILE_GUN_DOWN, SDLK_9},
    {TILE_GUN_LEFT, SDLK_0},
    {TILE_BUTTON_UNPRESSED, SDLK_o},
    {TILE_BRIDGE_VOID, SDLK_p},
};
#endif

/*
 * Rooms will be stored inside an array, where enums will help the developer identify them
 */
typedef enum
{
    ROOM_INVALID,
    ROOM_START,
    ROOM_MAZE,
    ROOM_SPIKES,
    ROOM_FLOATING_PLATFORMS,
    ROOM_DEADEND,
    ROOM_ONEWAY,
    ROOM_INDIANA_JONES,
    ROOM_BRIDGE_MAZE,
    ROOM_FINAL,
    TOTAL_ROOMS
} room_e;

typedef struct room_t
{
    // The resulting room textures will be stored here
    // This will save us from re-calculating the whole thing every single frame
    SDL_Texture *texture;

    // This will display every time a paper is encountered on the map
    // The game only supports one paper per room
    char paper_content[400];

    // Useful for one-time dialogues and special room-entering functionality for the plot
    bool has_been_visited;

    bool has_read_paper;
    vec2i_t paper_position;
    
    tile_e data[TILEMAP_WIDTH][TILEMAP_HEIGHT];
    room_e up, down, left, right;
} room_t;

/*
 * Creating some rooms and assigning them some data
 * You can modify these freely and re-compile the program
 */
static room_t rooms[TOTAL_ROOMS] = {
    [ROOM_START] = {
        .paper_content = "Dear captive,\n"
        "Welcome to robocracy. You've been jailed in this room for half a century and you're one of about ten humans alive. "
        "As I've promised - and robots do keep their promises - to figure out the first digit of the password, you just need to count the number of lines starting with the letter 'r'...\n"
        "  - Unit X381",
        
        .data = {
            {0, 0, 1, 1, 1, 1, 1, 3, 1, 1, TILE_PASSWORD_DOOR, 1, 1, 0, 0},
            {0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0},
            {0, 2, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 0},
            {2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2},
            {2, 2, 2, 2, 0, 2, 2, 2, 2, 2, 0, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 2, 2, 2, 2},
            {0, 2, 2, 2, 2, 2, 0, 2, 0, 2, 2, 2, 2, 2, 0},
            {0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0},
            {0, 2, 2, 2, 2, 2, 0, 2, 0, 2, 2, 2, 2, 2, 0},
            {2, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 0, 2, 2, 2, 2, 2, 0, 2, 2, 2, 2},
            {2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2},
            {0, 2, 0, 2, 2, 2, 2, 4, 2, 2, 2, 2, 0, 2, 0},
            {0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0},
            {0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0}
        },

        .up = ROOM_MAZE,
    },

    [ROOM_MAZE] = {
        .paper_content = "Dear dairy,\n"
        "My leg is wounded and I'm too old to get past those deadly spikes... \n"
        "I'm scared, the robot guards must be here any moment now and they won't hesitate to punish me. \n"
        "I'm proud of my vain attempt but I fear that I was humanity's last hope however egotistical that might sound.",
    
        .data = {
            {1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1},
            {2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 7, 2, 4},
            {2, 0, 0, 0, 0, 2, 0, 0, 0, 2, 0, 2, 0, 0, 0},
            {2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 0, 2, 2, 2, 0},
            {0, 0, 0, 2, 0, 0, 0, 2, 0, 2, 0, 0, 0, 2, 0},
            {2, 2, 2, 2, 0, 9, 0, 2, 2, 2, 0, 2, 2, 2, 0},
            {2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 0},
            {2, 2, 2, 9, 0, 2, 2, 2, 2, 0, 2, 2, 2, 2, 3},
            {2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0},
            {2, 2, 2, 2, 7, 7, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
            {2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 2, 2, 2},
            {7, 0, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0},
            {2, 2, 2, 9, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0}
        },

        .down = ROOM_START,
        .right = ROOM_SPIKES
    },

    [ROOM_SPIKES] = {
        .paper_content = "To anybody reading this,\n"
        "There are quite a lot of spikes in here, perhaps more than ten. "
        "If one wished to convert them into a single digit (say the third in a sequence), they could perform a simple addition. "
        "Anyway, why am I writing notes to myself again? Maybe the psychologist was right...",
        
        .data = {
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0},
            {2, 2, 2, 7, 2, 0, 0, 0, 2, 7, 2, 0, 0, 0, 2},
            {2, 2, 7, 7, 2, 9, 0, 2, 2, 7, 2, 2, 0, 2, 2},
            {2, 2, 0, 0, 2, 2, 0, 2, 2, 7, 2, 2, 7, 2, 2},
            {3, 2, 0, 0, 2, 2, 7, 2, 2, 0, 2, 2, 7, 2, 2},
            {2, 2, 0, 0, 2, 2, 7, 2, 2, 0, 2, 2, 7, 2, 2},
            {2, 2, 7, 7, 2, 2, 7, 2, 2, 0, 2, 2, 0, 2, 2},
            {2, 2, 2, 7, 2, 2, 7, 2, 0, 0, 9, 0, 0, 0, 2},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}
        },

        .left = ROOM_MAZE,
        .right = ROOM_FLOATING_PLATFORMS
    },

    [ROOM_FLOATING_PLATFORMS] = {
        .paper_content = "Fellow Human,\n"
        "If you're reading this you've already made a fair bit of progress! "
        "We managed to attain a hint about the second digit from an unsuspecting guard:\n"
        "'The second digit is infinity. Infinity is not a number, but I like the way it is written...'\n",

        .data = {
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {0, 0, 0, 0, 0, 0, 0, 11, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 11, 11, 0, 0, 11, 11, 4, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 11, 11, 11, 11, 0, 11, 0},
            {0, 0, 0, 0, 0, 0, 0, 11, 11, 0, 0, 0, 0, 11, 0},
            {2, 2, 2, 2, 2, 2, 7, 7, 7, 2, 2, 2, 2, 2, 2},
            {3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 7, 7, 7, 2, 2, 2, 2, 2, 2},
            {0, 0, 0, 0, 0, 0, 11, 0, 11, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 9, 11, 11, 11, 11, 11, 9, 0, 0, 0, 0},
            {0, 0, 0, 0, 11, 0, 0, 0, 0, 0, 11, 0, 0, 0, 0},
            {0, 0, 0, 9, 11, 0, 0, 0, 0, 0, 11, 9, 0, 0, 0},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        },

        .left = ROOM_SPIKES,
        .up = ROOM_DEADEND,
        .down = ROOM_ONEWAY
    },

    [ROOM_DEADEND] = {
        .paper_content = 0,
        .data = {
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 9, 2, 9, 2, 9, 2, 9, 2, 9, 2, 9, 2, 2},
            {2, 2, 2, 9, 2, 9, 2, 9, 2, 9, 2, 9, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0},
            {14, 0, 2, 2, 2, 7, 2, 2, 2, 7, 2, 2, 2, 0, 2},
            {14, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2},
            {2, 0, 2, 7, 2, 2, 2, 2, 2, 2, 2, 7, 2, 0, 2},
            {2, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 16},
            {2, 0, 2, 2, 2, 7, 2, 2, 2, 7, 2, 2, 2, 0, 16},
            {0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2},
        },

        .down = ROOM_FLOATING_PLATFORMS
    },

    [ROOM_ONEWAY] = {
        .paper_content = 0,
        .data = {
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0},
            {2, 0, 0, 0, 11, 11, 11, 0, 11, 11, 11, 0, 0, 0, 2},
            {2, 2, 0, 0, 11, 0, 0, 0, 11, 0, 11, 0, 0, 16, 2},
            {2, 9, 2, 11, 11, 0, 11, 11, 11, 0, 11, 0, 2, 2, 2},
            {9, 7, 9, 0, 11, 0, 11, 0, 0, 0, 11, 0, 2, 2, 2},
            {2, 2, 2, 11, 11, 0, 11, 0, 11, 11, 11, 0, 2, 2, 2},
            {14, 9, 0, 0, 11, 11, 11, 0, 11, 0, 0, 0, 0, 2, 2},
            {2, 0, 0, 0, 0, 0, 0, 0, 11, 11, 0, 0, 0, 0, 2},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 0, 0, 0, 0, 0},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        },

        .up = ROOM_FLOATING_PLATFORMS,
        .down = ROOM_INDIANA_JONES
    },

    [ROOM_INDIANA_JONES] = {
        .paper_content = 0,
        .data = {
            {1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 2, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0},
            {14, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0},
            {0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 9, 0, 2, 0, 0},
            {0, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0},
            {0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 16},
            {0, 0, 2, 0, 0, 0, 9, 0, 0, 0, 2, 0, 0, 0, 0},
            {0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {9, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 9},
            {9, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 9},
        },

        .up = ROOM_ONEWAY,
        .down = ROOM_BRIDGE_MAZE
    },

    [ROOM_BRIDGE_MAZE] = {
        .paper_content = 0,
        .data = {
            {1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {0, 0, 11, 0, 0, 0, 0, 0, 11, 0, 0, 0, 0, 0, 0},
            {11, 11, 11, 0, 11, 11, 11, 11, 11, 11, 11, 11, 11, 0, 0},
            {11, 0, 0, 0, 11, 0, 11, 0, 0, 0, 0, 0, 11, 0, 0},
            {11, 11, 11, 11, 11, 0, 11, 0, 11, 11, 11, 11, 2, 0, 0},
            {0, 0, 0, 0, 11, 0, 11, 0, 11, 0, 0, 0, 19, 0, 0},
            {17, 2, 11, 11, 11, 2, 11, 0, 11, 11, 11, 0, 2, 11, 11},
            {9, 2, 0, 0, 0, 19, 0, 0, 0, 0, 11, 0, 0, 0, 11},
            {0, 11, 0, 9, 9, 9, 11, 11, 11, 11, 11, 0, 11, 11, 11},
            {0, 11, 0, 9, 9, 9, 0, 0, 2, 0, 0, 0, 11, 0, 0},
            {0, 11, 0, 0, 0, 0, 0, 0, 19, 0, 0, 0, 11, 0, 0},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        },

        .up = ROOM_INDIANA_JONES,
        .down = ROOM_FINAL
    },

    [ROOM_FINAL] = {
        .paper_content = "I highly doubt that any fellow humans will be reading this but it's worth the try... "
        "I've been informed that the forth and last digit of the password is just the number of rooms located in this dungeon. "
        "Ugh! The last digit by itself is entirely useless! I wasn't lucky enough to figure out the rest. Humans are doomed.",
        
        .data = {
            {1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {7, 7, 0, 0, 2, 0, 0, 2, 0, 0, 2, 0, 0, 7, 7},
            {2, 2, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0, 2, 2},
            {7, 7, 0, 0, 2, 2, 0, 19, 0, 2, 2, 0, 0, 7, 7},
            {2, 2, 0, 0, 0, 2, 0, 4, 0, 2, 0, 0, 0, 7, 7},
            {7, 7, 0, 0, 2, 2, 0, 0, 0, 2, 2, 0, 0, 2, 2},
            {2, 2, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0, 7, 7},
            {7, 7, 0, 0, 2, 0, 2, 0, 2, 0, 2, 0, 0, 2, 2},
            {2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 9},
            {2, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 9},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
            {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2},
        },

        .up = ROOM_BRIDGE_MAZE,
    }
};

#endif
