/* Pioyi Engine 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _NG_TIMER_H
#define _NG_TIMER_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct
{
    bool is_active;
    uint32_t starting_time;
} ng_timer_t;

void ng_timer_start(ng_timer_t *timer);

uint32_t ng_timer_get_elapsed(ng_timer_t *timer);
// Just restarts the timer and returns the elapsed time in milliseconds
uint32_t ng_timer_restart(ng_timer_t *timer);

typedef struct
{
    uint32_t starting_time;
    
    // How long should it wait between each "event fire"
    uint32_t duration;
} ng_interval_t;

void ng_interval_create(ng_interval_t *interval, uint32_t duration);
bool ng_interval_is_ready(ng_interval_t *interval);

#endif
