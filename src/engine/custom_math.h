/* Pioyi Engine 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _NG_CUSTOM_MATH_H
#define _NG_CUSTOM_MATH_H

#include <stdbool.h>
#include <SDL2/SDL.h>

// A vector2f structure with some special methods that will be used throught most games
typedef struct
{
    float x, y;
} ng_vec2;

float ng_vector_get_magnitude(ng_vec2 *source);
void ng_vector_normalize(ng_vec2 *result, ng_vec2 *source);
void ng_vector_multiply_by(ng_vec2 *result, ng_vec2 *source, float scalar);

void ng_vectors_add(ng_vec2 *result, ng_vec2 *first, ng_vec2 *second);
void ng_vectors_substract(ng_vec2 *result, ng_vec2 *first, ng_vec2 *second);
void ng_vectors_multiply(ng_vec2 *result, ng_vec2 *first, ng_vec2 *second);
void ng_vectors_divide(ng_vec2 *result, ng_vec2 *first, ng_vec2 *second);

// Other math-related functions
bool ng_is_point_inside(SDL_Rect *rect, int x, int y);

int ng_get_distance(int x, int y, int other_x, int other_y);

#endif
