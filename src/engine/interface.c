/* Pioyi Engine
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "interface.h"
#include <SDL2/SDL.h>

static SDL_Color white = {255, 255, 255, 255};

void ng_label_create(ng_label_t *label, TTF_Font *font, unsigned int wrap_length)
{
    label->font = font;
    label->wrap_length = wrap_length;
}

void ng_label_set_content(ng_label_t *label, SDL_Renderer *renderer, const char *content)
{
    // Avoid the memory leak
    SDL_DestroyTexture(label->sprite.texture);
    
    SDL_Surface *surface = label->wrap_length > 0
        ? TTF_RenderText_Solid_Wrapped(label->font, content, white, label->wrap_length)
        : TTF_RenderText_Solid(label->font, content, white);

    ng_sprite_create(&label->sprite, SDL_CreateTextureFromSurface(renderer, surface));
    SDL_FreeSurface(surface);
}

void ng_label_destroy(ng_label_t *label)
{
    SDL_DestroyTexture(label->sprite.texture);
}
