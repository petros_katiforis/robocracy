/* Robocracy
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _NG_GAME_H
#define _NG_GAME_H

#include <SDL2/SDL.h>

// Just a wrapper around the most basic of SDL components
// Can be extended later on and gain more power
typedef struct
{
    SDL_Window *window;
    SDL_Renderer *renderer;

    int width, height;
} ng_game_t;

void ng_game_create(ng_game_t *game, const char *title, int width, int height);
void ng_game_destroy(ng_game_t *game);

#endif
