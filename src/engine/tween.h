/* Pioyi Engine 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _NG_TWEEN_H
#define _NG_TWEEN_H

#include <stdbool.h>

/*
 * Contains all the neccessary information to intorpolate the value smoothly
 * For now, it justs works with floats. In the future, a union can be introduced
 */
typedef struct
{
    bool is_active;
    float start, end;
    float *value;
    unsigned int duration, time_passed;
} ng_tween_t;

// All executing tweens will be held in this small list
// The game is small, it won't need many struct instances
static ng_tween_t tweens[20];
static unsigned int total_tweens = 0;

ng_tween_t* ng_tween_create(float *value);
void ng_tween_start(ng_tween_t *tween, float end, unsigned int duration);

// This function will return true if the tween just finished
bool ng_tween_update(ng_tween_t *tween, unsigned int delta);

#endif
