/* Pioyi Engine 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _NG_INTERFACE_H
#define _NG_INTERFACE_H

#include <stdbool.h>
#include "sprite.h"

// Labels are extended sprites
typedef struct
{
    ng_sprite_t sprite;

    TTF_Font *font;
    unsigned int wrap_length;
} ng_label_t;

// Leave wrap_length to 0 for default rendering in a single line
void ng_label_create(ng_label_t *label, TTF_Font *font, unsigned int wrap_length);

void ng_label_set_content(ng_label_t *label, SDL_Renderer *renderer, const char *content);
void ng_label_destroy(ng_label_t *label);

#endif
