/* Pioyi Engine 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "tween.h"

ng_tween_t* ng_tween_create(float *value)
{
    tweens[total_tweens].value = value;
    tweens[total_tweens].start = *value;
    total_tweens++;

    return &tweens[total_tweens - 1];
}

void ng_tween_start(ng_tween_t *tween, float end, unsigned int duration)
{
    // If it's still running we don't care. Just interpolate from where it was left running
    tween->start = *tween->value;

    tween->duration = duration;
    tween->end = end;
    tween->time_passed = 0;
    tween->is_active = true;
}

bool ng_tween_update(ng_tween_t *tween, unsigned int delta)
{
    tween->time_passed += delta;

    // Check if the tween has finished
    if (tween->time_passed > tween->duration)
    {
        *tween->value = tween->end;
        tween->is_active = false;
        return true;
    }

    *tween->value = tween->start + ((float) tween->time_passed / tween->duration) * (tween->end - tween->start);

    return false;
}
