/* Pioyi Engine 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _NG_SPRITE_H
#define _NG_SPRITE_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "custom_math.h"
#include "timers.h"

// Transforms are used for things that can be rendered
// They include a float based position, an SDL rectangle for rendering and an origin point
typedef struct
{
    ng_vec2 position;
    SDL_Rect rect;
} ng_transform_t;

void ng_transform_set_position(ng_transform_t *transform, float x, float y);
void ng_transform_set_x(ng_transform_t *transform, float x);
void ng_transform_set_y(ng_transform_t *transform, float y);

// Sprites are 2D entities that can be rendered using a simple texture
typedef struct
{
    SDL_Texture *texture;
    SDL_Rect src;

    ng_transform_t transform;
} ng_sprite_t;

void ng_sprite_create(ng_sprite_t *sprite, SDL_Texture *texture);
void ng_sprite_render(ng_sprite_t *sprite, SDL_Renderer *renderer);
void ng_sprite_set_scale(ng_sprite_t *sprite, float scale);

// Some sprites will have a texture consisting of multiple frames inside a larger texture atlas
void ng_sprite_create_with_frames(ng_sprite_t *sprite, SDL_Texture *texture, unsigned int total_frames);
void ng_sprite_set_frame(ng_sprite_t *sprite, int frame_index);

#endif
