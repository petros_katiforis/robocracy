/* Pioyi Engine
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sprite.h"
#include "common.h"

void ng_transform_set_position(ng_transform_t *transform, float x, float y)
{
    ng_transform_set_x(transform, x);
    ng_transform_set_y(transform, y);
}

// The reason I'm doing this is for more precision.
// Integers would result to rounding up and weird movement
void ng_transform_set_x(ng_transform_t *transform, float x)
{
    transform->position.x = x;
    transform->rect.x = (int) x;
}

void ng_transform_set_y(ng_transform_t *transform, float y)
{
    transform->position.y = y;
    transform->rect.y = (int) y;
}

void ng_sprite_create(ng_sprite_t *sprite, SDL_Texture *texture)
{
    if (!texture)
        ng_die("failed to create sprite, an invalid texture was provided");
    
    sprite->texture = texture;
    sprite->src.x = sprite->src.y = 0;

    // Fetching the texture's dimensions and saving them in the sprite's source rect 
    SDL_QueryTexture(texture, NULL, NULL, &sprite->src.w, &sprite->src.h);

    sprite->transform.rect.w = sprite->src.w;
    sprite->transform.rect.h = sprite->src.h;
}

void ng_sprite_set_scale(ng_sprite_t *sprite, float scale)
{
    sprite->transform.rect.w = sprite->src.w * scale;
    sprite->transform.rect.h = sprite->src.h * scale;
}

void ng_sprite_render(ng_sprite_t *sprite, SDL_Renderer *renderer)
{
    SDL_RenderCopy(renderer, sprite->texture, &sprite->src, &sprite->transform.rect);
}

void ng_sprite_create_with_frames(ng_sprite_t *sprite, SDL_Texture *texture, unsigned int total_frames)
{
    ng_sprite_create(sprite, texture);

    // Correcting the dimensions
    int frame_width = sprite->src.w / total_frames;
    sprite->src.w = sprite->transform.rect.w = frame_width;
}

void ng_sprite_set_frame(ng_sprite_t *sprite, int frame_index)
{
    sprite->src.x = sprite->src.w * frame_index;
}
