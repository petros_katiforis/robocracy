/* Pioyi Engine 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "audio.h"
#include "common.h"

Mix_Chunk* ng_audio_load(const char *file)
{
    Mix_Chunk *audio = Mix_LoadWAV(file);

    // Making sure that the audio file was successfully loaded
    if (!audio)
        ng_die("Something went wrong, couldn't load audio!");

    return audio;
}

void ng_audio_play(Mix_Chunk *audio)
{
    Mix_PlayChannel(-1, audio, 0);
}
