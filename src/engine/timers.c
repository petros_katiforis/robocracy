/* Pioyi Engine 
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "timers.h"
#include <SDL2/SDL.h>

void ng_timer_start(ng_timer_t *timer)
{
    timer->starting_time = SDL_GetTicks();
    timer->is_active = true;
}

uint32_t ng_timer_get_elapsed(ng_timer_t *timer)
{
    return SDL_GetTicks() - timer->starting_time;
}

uint32_t ng_timer_restart(ng_timer_t *timer)
{
    uint32_t now = SDL_GetTicks();
    uint32_t elapsed = now - timer->starting_time;

    // Restart the timer by making it count time since now
    timer->starting_time = SDL_GetTicks();

    return elapsed;
}

void ng_interval_create(ng_interval_t *interval, uint32_t duration)
{
    interval->duration = duration;
    interval->starting_time = SDL_GetTicks();
}

bool ng_interval_is_ready(ng_interval_t *interval)
{
    if (SDL_GetTicks() - interval->starting_time > interval->duration)
    {
        // If the interval has been reached, restart the timer and return true
        interval->starting_time = SDL_GetTicks();

        return true;
    }

    return false;
}
