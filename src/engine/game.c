/* Robocracy
 * Copyright (C) 2023 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "game.h"
#include "common.h"
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <time.h>

void ng_game_create(ng_game_t *game, const char *title, int width, int height)
{
    // Getting pseudo-randomness working
    srand(time(NULL));
    
    // Initializing SDL components
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        ng_die("failed to initialize SDL2");
    
    if (IMG_Init(IMG_INIT_PNG) < 0)
        ng_die("failed to initialize SDL2/SDL_image");

    if (TTF_Init() < 0)
        ng_die("failed to initialize SDL2/SDL_ttf");

    // Initializing SDL_mixer with the standard settings
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
        ng_die("failed to open audio device and initialize SDL_Mixer");
    
    game->width = width;
    game->height = height;
    
    // Creating the window at the center of the screen with the specified properties
    game->window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                    width, height, SDL_WINDOW_SHOWN);

    if (!game->window)
        ng_die("failed to create the default SDL2 window");
    
    game->renderer = SDL_CreateRenderer(game->window, -1, SDL_RENDERER_ACCELERATED);
}

// Clearing up all SDL components
void ng_game_destroy(ng_game_t *game)
{
    SDL_DestroyRenderer(game->renderer);
    SDL_DestroyWindow(game->window);

    SDL_Quit();
    IMG_Quit();
    TTF_Quit();
}
